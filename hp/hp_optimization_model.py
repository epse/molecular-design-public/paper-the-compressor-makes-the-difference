from hp import hp_process_model
from molecule import state_equation, constraints
from feos.si import *
from knitro import *
import numpy as np
import os

####################################################################################################################
####################################################################################################################
#############################################  FUNCTIONS ###########################################################
####################################################################################################################
####################################################################################################################


def set_up_optimization(mode, x_init, integer_cuts, process_parameters, compressor_parameters, camd_parameters):

    # CALLBACK FUNCTIONS

    # Callback function is called in each iteration to evaluate non-linear (black-box)
    # objective function and constraints

    def callback_eval_hp(kc, cb, evalRequest, evalResult, userParams):
        if evalRequest.type != KN_RC_EVALFC:
            print("*** callbackEvalF incorrectly called with eval type %d" %
                  evalRequest.type)
            return -1
        x = evalRequest.x

        # Create saft parameters and eos
        #joback_parameters = []
        try:
            saft_parameters, sp_values, jp_values = state_equation.create_parameters(
                x, process_parameters, camd_parameters)
            eos = state_equation.create_eos(saft_parameters)

            # Calculate heat pump process
            COP, constraints, _, _, _, _, _, _ = hp_process_model.process_model(
                x, eos, sp_values, jp_values, process_parameters, compressor_parameters)

        except:

            # Alternative values

            COP = 0
            constraints = [-0.1 for i in range(process_parameters.n_compression_con +
                                               process_parameters.n_pinch_con + process_parameters.n_pressure_con)]

        # Objective function
        evalResult.obj = COP

        # Constraints
        for i, c in enumerate(constraints):
            evalResult.c[i] = c

        return 0

    # Create a new Knitro solver instance.
    try:
        kc = KN_new()
    except:
        print("Failed to find a valid license.")
        quit()

    ## VARIABLES ##

    # T_evap = x[0]
    # T_cond = x[1]
    # deltaT_sh = x[2]
    # deltaT_sc = x[3]
    # d/d_0 = x[4] : compressor diameter
    # x[5] ... x[10]: binary variables to indicate molecular structure
    # x[11] ... x[32]: integer variables to indicate respective number of segments

    # number of variables
    n = process_parameters.n_pdof + camd_parameters.n_struct + camd_parameters.n_seg
    # Add variables
    KN_add_vars(kc, n)

    # If fixed mode, change default bounds
    if mode == "Fixed":
        camd_parameters = constraints.fix_molecule(
            x_init, camd_parameters, process_parameters)

    # Set upper and lower bounds
    # Process DoF
    xLoBnds = process_parameters.xLoBnds
    xUpBnds = process_parameters.xUpBnds
    # For constant compressor efficiency: fix diameter
    if compressor_parameters.model == "constant":
        xLoBnds[4] = 3
        xUpBnds[4] = 3
    # Structures
    yLoBnds = camd_parameters.yLoBnds
    yUpBnds = camd_parameters.yUpBnds
    # Segments
    nLoBnds = camd_parameters.nLoBnds
    nUpBnds = camd_parameters.nUpBnds

    # Set bounds
    if mode == "Fixed":
        indVarBnds = [i for i in range(
            process_parameters.n_pdof+camd_parameters.n_struct+camd_parameters.n_seg)]
        KN_set_var_lobnds(kc, indexVars=indVarBnds, xLoBnds=[
            *xLoBnds, *yLoBnds, *nLoBnds])
        KN_set_var_upbnds(kc, indexVars=indVarBnds, xUpBnds=[
            *xUpBnds, *yUpBnds, *nUpBnds])
    else:
        indVarBnds = [i for i in range(
            process_parameters.n_pdof+camd_parameters.n_struct+camd_parameters.n_seg)]
        KN_set_var_upbnds(kc, indexVars=indVarBnds, xUpBnds=[
                          *xUpBnds, *yUpBnds, *nUpBnds])
        KN_set_var_lobnds(kc, indexVars=indVarBnds, xLoBnds=[
            *xLoBnds, *yLoBnds, *nLoBnds])

    # Set variable types. Default: continuous, thus they only have to be modified for y (binary) and n (integer)
    index_molecule = [*[camd_parameters.y[s] for s in camd_parameters.all_structures],
                      *[camd_parameters.n[s] for s in camd_parameters.all_segments]]
    if mode == "MINLP":
        KN_set_var_types(kc, indexVars=index_molecule, xTypes=[*[KN_VARTYPE_BINARY for i in range(
            camd_parameters.n_struct)], *[KN_VARTYPE_INTEGER for i in range(camd_parameters.n_seg)]])
    # Set initial values for variables
    if x_init:
        if len(x_init) == n:
            KN_set_var_primal_init_values(kc, xInitVals=x_init)
        else:
            print(
                "Error! Number of values in initial vector does not agree with number of variables.")

    ## CONSTRAINTS ##

    # Number of constraints m: process, pinch, compression, CAMD constraints and additional integer cut constraints (only for MINLP)
    m = process_parameters.n_process_con + process_parameters.n_pinch_con + process_parameters.n_compression_con + \
        process_parameters.n_pressure_con + process_parameters.n_heating_pow_con + \
        camd_parameters.n_camd_constraints + len(integer_cuts)
    # Add constraints
    KN_add_cons(kc, m)
    # Set lower bounds for process, pinch and compression constraints.
    # Bounds for CAMD constraints are set within the CAMD formulation.
    # Constraint indices
    # only process constraints (no CAMD, integer cut and saft constraints)
    iPrCon = [i for i in range(process_parameters.n_process_con + process_parameters.n_pinch_con +
                               process_parameters.n_compression_con + process_parameters.n_pressure_con)]
    # only constraints evaluated in callback: pinch, compression and pressure constraints, heating power constraint
    iCbCon = [i for i in range(process_parameters.n_process_con, process_parameters.n_process_con + process_parameters.n_pinch_con +
                               process_parameters.n_compression_con + process_parameters.n_pressure_con + process_parameters.n_heating_pow_con)]  # index of constraints evaluated in callback
    # heating power constraint
    iHeatCon = [process_parameters.n_process_con + process_parameters.n_pinch_con +
                process_parameters.n_compression_con + process_parameters.n_pressure_con]
    # Lower bounds for all constraints except CAMD constraints are set
    # Process constraints
    c_process_loBnds = np.zeros(process_parameters.n_process_con)
    # Pinch constraints
    c_pinch_loBnds = np.ones(process_parameters.n_pinch_con) * \
        (process_parameters.DeltaT["min"] / KELVIN)
    # Compression constraints
    c_comp_loBnds = np.zeros(process_parameters.n_compression_con)
    # Pressure constraints
    c_pressure_loBnds = np.zeros(process_parameters.n_pressure_con)
    # Vector with lower bounds
    lobounds = [*c_process_loBnds, *c_pinch_loBnds,
                *c_comp_loBnds, *c_pressure_loBnds]
    # Set lower bounds.
    KN_set_con_lobnds(kc, indexCons=iPrCon, cLoBnds=lobounds)
    # Equality bound for heating power is set
    KN_set_con_lobnds(kc, indexCons=iHeatCon, cLoBnds=[-0.02])
    KN_set_con_upbnds(kc, indexCons=iHeatCon, cUpBnds=[0.02])
    # "Process constraint": add linear constraint for T_cond > T_evap as constraint with index 0
    KN_add_con_linear_struct(kc, [0, 0], [0, 1], [-1, 1])

    # Add CAMD constraints.
    kc = constraints.camd_formulation(
        kc, integer_cuts, process_parameters, camd_parameters)

    ## CALLBACK ##

    # Create callback.
    cb = KN_add_eval_callback(
        kc, evalObj=True, indexCons=iCbCon, funcCallback=callback_eval_hp)

    ## MAXIMIZATION ##

    # Set maximization.
    KN_set_obj_goal(kc, KN_OBJGOAL_MAXIMIZE)

    ## PARAMETERS ##

    if mode == "MINLP":
        KN_load_param_file(kc, "options_minlp.opt")
    else:
        KN_load_param_file(kc, "options.opt")

    return kc
