from molecule import state_equation
from hp import hp_process_model, hp_parameters
from knitro import *
from feos.si import *
import json
import xlsxwriter
import numpy as np


def get_solution(kc):

    # Print solution information
    nStatus, objSol, x, lambda_ = KN_get_solution(kc)
    tcpu = KN_get_solve_time_cpu(kc)
    treal = KN_get_solve_time_real(kc)

    return x, objSol, lambda_, tcpu, treal


def print_solution(x, camd_parameters, process_parameters):

    # Print solution information
    print("Optimal x")
    for i in range(process_parameters.n_pdof):
        print("  x[%d] = \t %e" % (i, x[i]), " K")
    for s in camd_parameters.all_structures:
        if x[camd_parameters.y[s]] > 1e-10:
            print("  " + s[0:4] + " \t\t %e " % (x[camd_parameters.y[s]]))
    for s in camd_parameters.all_segments:
        if x[camd_parameters.n[s]] > 1e-6:
            if len(s) < 6:
                print("  " + s + "    \t %e " % (x[camd_parameters.n[s]]))
            else:
                print("  " + s + "\t %e " % (x[camd_parameters.n[s]]))


def save_solution(x, filename):

    # Write solution to json file
    filename_here = "./" + filename
    x_save = json.dumps(x)
    open(filename_here, "w").write(x_save)


def read_solution(filename):

    filename_here = "./" + filename
    with open(filename_here) as f:
        x = json.load(f)

    return x


def simulate_hp_process(x, process_parameters, compressor_parameters, camd_parameters):

    # Create saft parameters and eos
    saft_parameters, sp_values, jp_values = state_equation.create_parameters(
        x, process_parameters, camd_parameters)
    eos = state_equation.create_eos(saft_parameters)

    # Call process model
    COP, _, states, eta_sC, Q_cond, m_dot, n_piston, Q_vol = hp_process_model.process_model(
        x, eos, sp_values, jp_values, process_parameters, compressor_parameters)
    print("COP: " + str(COP))
    print("eta_sC: " + str(eta_sC))
    return COP, states, eos, eta_sC, Q_cond, m_dot, n_piston, Q_vol


def get_segment_string(x, camd_parameters, process_parameters):

    segments = camd_parameters.all_segments

    size_x = len(x)
    start = process_parameters.n_pdof + camd_parameters.n_struct
    end = process_parameters.n_pdof + camd_parameters.n_struct + camd_parameters.n_seg

    x_mol = x[start:end]

    # Get segment strings
    # Create string with all segments
    string = ""
    for i, s in enumerate(x_mol):
        if s > 1e-6:
            if s - int(s) == 0:
                string = string + " " + str(int(s)) + "x " + segments[i]
            else:
                string = string + " " + \
                    str(np.round(s, 2)) + "x " + segments[i]

    return string
