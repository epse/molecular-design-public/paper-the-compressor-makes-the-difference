import json, os
import numpy as np
import pandas as pd
import openpyxl

def get_molecule_name(x, process_parameters, camd_parameters):
    """     Function extracts name of molecule from database of known molecules
            if segment configuration is given.  
            
            Input:  - variables x,
                    - process parameters,
                    - camd parameters

            Returns: molecule name
            
            """    

    # Get part of x that describes the segment configuration
    first_seg = camd_parameters.all_segments[0]
    ind_first_seg = camd_parameters.n[first_seg]
    last_seg = camd_parameters.all_segments[-1]
    ind_last_seg = camd_parameters.n[last_seg]
    x_mol = x[ind_first_seg:(ind_last_seg+1)]

    # Read database
    dfs = pd.read_excel("molecule/parameters_homo_segments.xlsx", sheet_name=None)['Database']
    matches = []

    # Scan dataframe for segment configuration
    for label, col in dfs.items():
        if label != "name" and label != "identifier":
            temp = []
            for i in col:
                if np.isnan(i):
                    i = 0.0
                temp.append(i)
            diff = [abs(value - x_mol[j]) for j, value in enumerate(temp)]
            if sum(diff) < 1e-5:
                matches.append(label)
    
    # Check matches
    if len(matches) == 1:
        molecule_name = matches[0]
    elif len(matches) == 0:
        molecule_name = update_database_from_segments(x_mol, camd_parameters)
    else:
        string = ""
        for j in matches:
            string = string + ", " + j
        raise Exception("Found more than one matches. Please check database. Matches:" + string)        

    return molecule_name

def update_database_from_segments(x, camd_parameters):
    """ Function updates database if a name to a known segment configuration is not contained yet.

        Input:  
                - x that only contains info about segments
                - camd parameters

        Returns: name of molecule

    """

    # List with all segments
    segments = camd_parameters.all_segments

    # Create string with all segments
    string =  ""
    for i, s in enumerate(x):
        if s > 0:
            string = string + " " + str(int(s)) + segments[i]
    
    # Get input
    name = input("Please enter name for the following molecule: " + string)
    # Catch exception for too short input
    if len(name) < 4:
        raise Exception("Entered molecule name is too short, there must be a typo.")
    
    # Open Workbook 
    wb = openpyxl.load_workbook("molecule/parameters_homo_segments.xlsx")
    sheet = wb['Database']

    # Maximum columns
    max_col = sheet.max_column

    # Write name to top row
    sheet.cell(row=1, column=max_col+1).value = name

    # Write values to remaining rows
    for i, value in enumerate(x):
        if value > 0:
            sheet.cell(row=i+2, column=max_col+1).value = value

    # Save workbook
    wb.save("molecule/parameters_homo_segments.xlsx")

    # Print success
    print("Database updated successfully.")

    return name

def get_segment_config_from_name(name, camd_parameters):

    # Read database
    dfs = pd.read_excel("molecule/parameters_homo_segments.xlsx", sheet_name=None)['Database']
    matches = []

    # Scan dataframe for segment configuration
    for label, col in dfs.items():
        if label == name:
            temp = []
            for i in col:
                if np.isnan(i):
                    i = 0.0
                temp.append(i)
            matches.append(temp)

    # Check matches
    if len(matches) == 1:
        segments = matches[0]
    elif len(matches) == 0:
        segments = update_database_from_name(name, camd_parameters)
    else:
        raise Exception("Found more than one match. Please check database and delete respective double columns.")        

    return segments

def update_database_from_name(name, camd_parameters):

    # We don't know your molecule yet.
    yno = input("We do not know your molecule yet. Do you want to add it to the database? Enter y to add it, press any other key to cancel.")
    if yno == "y":
        # Get molecular structure from user
        keepon = True
        segment_list = []
        segment_no = []
        while keepon:
            temp_seg = input("Enter next segment name. If you're finished, just press enter.")
            if temp_seg == "":
                keepon = False
                break
            temp_no = input("Enter amount of segment " + temp_seg + ".")
            if temp_no == "":
                raise Exception("No amount of segment " + temp_seg + " entered. Please try again.")
            segment_list.append(temp_seg)
            segment_no.append(int(temp_no))

        # Check if there are enough segments (at least 3)
        total_seg = 0
        for v in segment_no:
            total_seg += v
        if total_seg < 1:
            raise Exception("Entered segment configuration may be wrong, because there are less than 1 segments.")

        # Build x in the right order.
        all_segments = camd_parameters.all_segments
        x = [0 for i in range(camd_parameters.n_seg)]
        for i, s in enumerate(segment_list):
            # Check if s in list of all segments
            if s in all_segments:
                # add number of segment to x 
                index = all_segments.index(s)
                x[index] = segment_no[i]
            else:
                raise Exception("Your entered segment " + s + " does not seem to be a valid segment. Please revise.")

        # Open Workbook 
        wb = openpyxl.load_workbook("molecule/parameters_homo_segments.xlsx")
        sheet = wb['Database']

        # Maximum columns
        max_col = sheet.max_column

        # Write name to top row
        sheet.cell(row=1, column=max_col+1).value = name

        # Write values to remaining rows
        for i, value in enumerate(x):
            if value > 0:
                sheet.cell(row=i+2, column=max_col+1).value = value

        # Save workbook
        wb.save("molecule/parameters_homo_segments.xlsx")

        # Print success
        print("Database updated successfully.")

    else:
        raise Exception("Ouch. Something went wrong.")

    return x