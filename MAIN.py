from molecule import camd_parameters, state_equation
from functions import post_processing
from hp import hp_optimization_model, hp_parameters
from knitro import *
import os
from molecule import database
from datetime import datetime

from feos import *
from feos.si import *


####################################################################################################################
####################################################################################################################
############################################### USER OPTIONS #######################################################
####################################################################################################################
####################################################################################################################

# Specify technology to optimize
tech = "hp"

# Do you want to fix molecule? Molecule will be fixed to molecules in the list molecules_fixed.
calc_fixed_molecule = 1  # 1 = yes, 0 = no
molecules_fixed = ["Propane"]

# Do you want to calculate target (NLP) or read from file 'results/target.json'?
calc_target = 0  # 1 = calculate, 0 = read from file

# Do you want to calculate fluid ranking (MINLP)?
calc_ranking = 0  # 1 = calculate

# Specify if integer cuts shall be read from file -> 
read_int_cut_from_file = 0

# Do you want to save solution to a .json file?
# Default: NLP as target.json and MINLP as ranking_x.json.
save_solution = 1

# Which initial molecule do you want to use for the NLP?
molecule = "Propane"

# Specify number of fluids for fluid ranking
fluid_number = 10

# Specify compressor model: "constant" or "detailed"
compressor_model = "constant"
compressor_model = "detailed"

# Specify name of .json files.
# Target -> specify name of target file only if target is not computed, but ranking is
filename_target = ""

# Ranking/Integer Cuts -> needed if integer cuts are read from file
filename_int_cut = ""
# Initial values for detailed compressor optimization 
filename_init_values = ""


####################################################################################################################
####################################################################################################################
############################################### PARAMETERS #########################################################
####################################################################################################################
####################################################################################################################

# Load parameters
camd_param = camd_parameters.camd_parameters(tech)
if tech == "hp":
    process_parameters = hp_parameters.process_parameters()
    compressor_parameters = hp_parameters.compressor_parameters(
        compressor_model)
else:
    print("Could not find technology to optimize.")

# Create string for results directory
if calc_fixed_molecule == 1:
    mode = "PD_fixed_fluid"
elif (calc_target == 1 and calc_ranking == 0) or (calc_target+calc_fixed_molecule+calc_ranking+read_int_cut_from_file == 0):
    mode = "Target"
elif calc_ranking == 1 or read_int_cut_from_file:
    mode = "Ranking"

# Find out current time to later save results
now = datetime.now()
time_string = now.strftime("%Y-%m-%d_%H-%M")
dir_mode = "results/" + tech + "/" + mode + "/"
dir_logs = "logs/" + tech + "/" + mode + "/" + time_string + "/"
dir_results = dir_mode + time_string + "/"
dir_figures = dir_results + "figures/"

####################################################################################################################
####################################################################################################################
########################### NON LINEAR PROGRAM: PROCESS DESIGN WITH FIXED MOLECULE #################################
####################################################################################################################
####################################################################################################################


if calc_fixed_molecule == 1:

    x_fixed = {}
    # Perform process optimization with fixed molecule
    for molecule in molecules_fixed:

        # Get fixed molecule
        x_init_nlp = state_equation.get_x_init(
            molecule, process_parameters, camd_param)

        # For detailed compressor model, initialize process degrees of freedom with values from simplified optimization:
        if compressor_parameters.model == "detailed":
            try:
                x_pdof = post_processing.read_solution(
                    filename_init_values)  # take last x
                for i in range(process_parameters.n_pdof):
                    x_init_nlp[i] = x_pdof[i]
                x_init_nlp[4] = 3.5
            except:
                pass

        # Set up optimization problem.
        if tech == "hp":
            kc_fixed = hp_optimization_model.set_up_optimization(
                "Fixed", x_init_nlp, [], process_parameters, compressor_parameters, camd_param)

        # Solve the problem.
        status = KN_solve(kc_fixed)

        # Solution information.
        x_fixed[molecule], objSol_fixed, lambda_fixed, tcpu_fixed, treal_fixed = post_processing.get_solution(
            kc_fixed)

        # Save solution in json file and save log if desired
        if save_solution == 1:
            if not os.path.exists(dir_results):
                os.makedirs(dir_results)
            post_processing.save_solution(
                x_fixed[molecule], dir_results + "pd_" + molecule + ".json")
            os.rename("knitro.log", dir_results + "pd_" + molecule + ".log")

        # Delete the Knitro solver instance.
        KN_free(kc_fixed)

####################################################################################################################
####################################################################################################################
######################################## NON LINEAR PROGRAM: TARGET ################################################
####################################################################################################################
####################################################################################################################

# Calculate target only if desired
if calc_target == 1:
    # Calculate target

    # Get initial molecule
    x_init_nlp = state_equation.get_x_init(
        molecule, process_parameters, camd_param)

    # Better initial point
    if compressor_parameters.model == "detailed":
        x_pdof = post_processing.read_solution(
            filename_target)  # take last x c
        for i in range(process_parameters.n_pdof):
            x_init_nlp[i] = x_pdof[i]

    # Set up optimization problem.
    if tech == "hp":
        kc_nlp = hp_optimization_model.set_up_optimization(
            "NLP", x_init_nlp, [], process_parameters, compressor_parameters, camd_param)

    # Solve the problem.
    status = KN_solve(kc_nlp)

    # Solution information.
    x_nlp, objSol_nlp, lambda_nlp, tcpu_nlp, treal_nlp = post_processing.get_solution(
        kc_nlp)

    # Save solution in json file and save log if desired
    # if save_solution == 1 and status == 0:
    if save_solution == 1:
        if not os.path.exists(dir_results):
            os.makedirs(dir_results)
        post_processing.save_solution(x_nlp, dir_results + "target.json")
        os.rename("knitro.log", dir_results + "target.log")

    # Delete the Knitro solver instance.
    KN_free(kc_nlp)

####################################################################################################################
####################################################################################################################
############################### MIXED-INTEGER NON LINEAR PROGRAM: FLUID RANKING ####################################
####################################################################################################################
####################################################################################################################

# x_prev: solution vector with previous solutions
x_prev = []
if read_int_cut_from_file == 1:
    x_prev = post_processing.read_solution(filename_int_cut)
# Determine start ranking position
start = len(x_prev)

# If target is not calculated in same run, read from file 'results/target.json'
if calc_target == 0 and calc_fixed_molecule == 0:
    x_nlp = post_processing.read_solution(filename_target)

# Solve routine: solve repeatedly with added integer cuts to get ranking
# integer_cuts is a list containing all previous solutions
all_x = x_prev

# Calculate fluid ranking if desired.
if calc_ranking == 1:

    for run in range(start, fluid_number):

        # Set up optimization problem
        if tech == "hp":
            kc_minlp = hp_optimization_model.set_up_optimization(
                "MINLP", x_nlp, all_x, process_parameters, compressor_parameters, camd_param)

        # Solve the problem.
        status = KN_solve(kc_minlp)

        # Solution information.
        x_minlp, _, _, _, _ = post_processing.get_solution(kc_minlp)

        # Get name of molecule (switch on for debugging purposes)
        # name = database.get_molecule_name(x_minlp, process_parameters, camd_param)

        # Add solution information to solution collection lists.
        all_x.append(x_minlp)

        # Delete the Knitro solver instance.
        KN_free(kc_minlp)

        # Save solution in json file if desired
        if save_solution == 1:
            if not os.path.exists(dir_results):
                os.makedirs(dir_results)
            post_processing.save_solution(
                all_x, dir_results + "ranking" + str(run+1) + ".json")
            os.rename('knitro.log', dir_results +
                      'log_ranking' + str(run+1) + '.log')
            os.rename('kdbg_mip.log', dir_results +
                      'mip' + str(run+1) + '.log')

        # Stop runs if previous optimization has not been successful
        # if status != 0:
        #    break

####################################################################################################################
####################################################################################################################
############################################## POST-PROCESSING #####################################################
####################################################################################################################
####################################################################################################################

## PD WITH FIXED MOLECULE ##
if calc_fixed_molecule == 1:

    for molecule in molecules_fixed:
        # Print solution.
        print("PD with " + molecule + ":")
        post_processing.print_solution(
            x_fixed[molecule], camd_param, process_parameters)
        if tech == "hp":
            # Simulate process.
            COP_fixed, states_fixed, eos_fixed, eta_sC_fixed, Q_cond_fixed, m_dot_fixed, n_piston_fixed, Q_vol_fixed = post_processing.simulate_hp_process(
                x_fixed[molecule], process_parameters, compressor_parameters, camd_param)

# TARGET OPTIMIZATION

if calc_fixed_molecule == 0:
    # Print solution.
    print("Target optimization:")
    post_processing.print_solution(
        x_nlp, camd_param, process_parameters)
    if tech == "hp":
        # Simulate process.
        COP_nlp, states_nlp, eos_nlp, eta_sC_nlp, Q_cond_nlp, m_dot_nlp, n_piston_nlp, Q_vol_nlp = post_processing.simulate_hp_process(
            x_nlp, process_parameters, compressor_parameters, camd_param)

## MINLP ##

# for each run
if calc_ranking == 1 or read_int_cut_from_file == 1:

    if tech == "hp":

        all_COP = []
        all_states = {}
        all_eos = {}
        all_Q = []
        all_eta = []
        all_names = []
        all_m = []
        all_n_piston = []
        all_Q_vol = []

        for count, x_run in enumerate(all_x):

            # Print solution.
            # Get molecule name:
            name = database.get_molecule_name(
                x_run, process_parameters, camd_param)
            print("Fluid ranking, Candidate " + str(count+1) + ", " + name)
            post_processing.print_solution(
                x_run, camd_param, process_parameters)
            # Simulate process.
            COP_minlp, states_minlp, eos_minlp, eta_sC_minlp, Q_cond_minlp, m_dot_minlp, n_piston_minlp, Q_vol_minlp = post_processing.simulate_hp_process(
                x_run, process_parameters, compressor_parameters, camd_param)
            
            # Save COPs, states and EoS
            all_COP.append(COP_minlp)
            all_states[count] = states_minlp
            all_eos[count] = eos_minlp
            all_Q.append(Q_cond_minlp)
            all_eta.append(eta_sC_minlp)
            all_names.append(name)
            all_m.append(m_dot_minlp)
            all_n_piston.append(n_piston_minlp)
            all_Q_vol.append(Q_vol_minlp)

    end = datetime.now()

    timedelta = end - now
