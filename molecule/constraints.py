from knitro import *
import numpy as np


def camd_formulation(kc, integer_cuts, process_parameters, camd_parameters):

    # Variable pointers
    y = camd_parameters.y
    n = camd_parameters.n

    # Numbers of structures, segments
    n_seg = camd_parameters.n_seg
    n_struct = camd_parameters.n_struct
    n_tot_max = camd_parameters.n_tot_max
    n_tot_min = camd_parameters.n_tot_min

    # Structures, segments
    structures = camd_parameters.all_structures
    allowed_structures = camd_parameters.allowed_structures
    segments = camd_parameters.all_segments
    segment_param = camd_parameters.segment_param
    molecules = camd_parameters.molecules

    ## CONSTRAINTS ##
    # Find out start of constraint index
    try:
        count = process_parameters.n_process_con + process_parameters.n_pinch_con + \
            process_parameters.n_compression_con + \
            process_parameters.n_pressure_con + process_parameters.n_heating_pow_con
    except:
        count = process_parameters.n_process_con + \
            process_parameters.n_pinch_con + process_parameters.n_turbine_con

    # Only one certain type of molecular structure can be active
    iCon = [count for n in range(n_struct)]
    iVar = [y[s] for s in structures]
    coefs = np.ones(n_struct)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=1)
    count += 1

    # Connect molecular structure and segments
    # Alkenes
    iCon = [count for n in range(4)]
    iVar = [n["dCH2"], n["dCH"], n["dC<"], y["alkene"]]
    coefs = [1, 1, 1, -2]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Aromatics
    iCon = [count for n in range(3)]
    iVar = [n["CH_arom"], n["C_arom"], y["arom"]]
    coefs = [1, 1, -6]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Hexanes
    iCon = [count for n in range(3)]
    iVar = [n["CH2_hex"], n["CH_hex"], y["hex"]]
    coefs = [1, 1, -6]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Pentanes
    iCon = [count for n in range(3)]
    iVar = [n["CH2_pent"], n["CH_pent"], y["pent"]]
    coefs = [1, 1, -5]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Single molecules
    iCon = [count for n in range(len(molecules) + 1)]
    iVar = [*[n[m] for m in molecules], y["M"]]
    coefs = [*[1 for n in range(len(molecules))], -1]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Alkyl side groups
    A = ['CH3', 'CH2', '>CH', '>C<']
    # alkyl side groups only exist if a branch exists in the ring
    iCon = [count for n in range(len(A) + 6)]
    iVar = [*[y["alkane"], y["alkene"], y["P"]], *[n[a]
                                                   for a in A], *[n["CH_arom"], n["CH_hex"], n["CH_pent"]]]
    coefs = [*[n_tot_max, n_tot_max, n_tot_max], *
             [-1 for a in A], *[n_tot_max, n_tot_max, n_tot_max]]
    lo = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Triple/polar/associating bond groups
    P = ["CtCH", "CHdO", ">CdO", "HCOO", "OCH3", "OCH2", "COO", "OH", "NH2"]
    iCon = [count for n in range(len(P) + 1)]
    iVar = [*[n[p] for p in P], *[y["P"]]]
    coefs = [*[1 for p in P], *[-1]]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Ensure proper connectivity: octet rule
    iCon = [count for n in range(n_seg + 4)]
    iVar = [*[n[s] for s in segments], *
            [y["alkane"], y["alkene"], y["P"], y["M"]]]
    coefs = [*[(2 - segment_param[s]["n_openBonds"])
               for s in segments], *[-2, -2, -2, -2]]
    eq = 0
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=eq)
    count += 1

    # Enforce a single connected molecule
    for s in segments:
        iCon = [count for n in range(n_seg + 3)]
        iVar = [*[n[j] for j in segments], *[y["alkane"], y["alkene"], y["P"]]]
        seg_coefs = []
        lo = 0
        for j in segments:
            if j == s:
                seg_coefs.append(2 - segment_param[s]["n_openBonds"])
            else:
                seg_coefs.append(1)
        coefs = [*seg_coefs, *[-2, -2, -2]]
        KN_add_con_linear_struct(
            kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
        KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
        count += 1

    # Forbid CH3-OCH2-CH3 to enforce CH3O-CH2-CH3
    iCon = [count for n in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 0
    for s in segments:
        if s == "CH3":
            coefs.append(0)
        elif s == "OCH2":
            coefs.append(-1)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Ethane: molecule cannot only consist of CH3 groups
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Ethylene: no more than 1 dCH2 group
    KN_add_con_linear_struct(kc, indexCons=count, indexVars=n["dCH2"], coefs=1)
    KN_set_con_upbnds(kc, indexCons=count, cUpBnds=1)
    count += 1

    # Forbid combined molecule for single molecules
    # Acetone: if there is CH3 and >CdO, there must at least be 1 other group
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3" or s == ">CdO":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Acetaldehyde: if there is CH3 and CHdO, there must at least be 1 other group
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3" or s == "CHdO":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Dimethyl ether: if there is CH3 and OCH3, there must at least be 1 other group
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3" or s == "OCH3":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Methyl formate: if there is CH3 and HCOO, there must at least be 1 other group
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3" or s == "HCOO":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Methyl acetate: if there is CH3 and COO, there must at least be 1 other group
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3" or s == "COO":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Forbid combined molecule for single molecules
    # Propyne: if there is CH3 and CtCH, there must at least be 1 other group
    iCon = [count for s in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = []
    lo = 1
    for s in segments:
        if s == "CH3" or s == "CtCH":
            coefs.append(0)
        else:
            coefs.append(1)
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    count += 1

    # Maximum and minimum amount of segments
    iCon = [count for n in range(n_seg)]
    iVar = [n[s] for s in segments]
    coefs = np.ones(n_seg)
    lo = n_tot_min
    up = n_tot_max
    KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=coefs)
    KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lo)
    KN_set_con_upbnds(kc, indexCons=count, cUpBnds=up)
    count += 1

    # Integer cut constraints
    for cut in integer_cuts:
        # Forbid single molecules separately, first: combined molecules
        if cut[y["M"]] == 0:
            iCon = [count for n in range(n_seg)]
            iVar = [n[s] for s in segments]
            qcoefs = [1 for n in range(n_seg)]
            lcoefs = [-2*cut[n[s]] for s in segments]
            lbnds = 0.5-1*(sum(cut[n[s]]**2 for s in segments))

            KN_add_con_quadratic_struct(
                kc, indexCons=iCon, indexVars1=iVar, indexVars2=iVar, coefs=qcoefs)
            KN_add_con_linear_struct(
                kc, indexCons=iCon, indexVars=iVar, coefs=lcoefs)
            KN_set_con_lobnds(kc, indexCons=count, cLoBnds=lbnds)
            count += 1
        elif cut[y["M"]] > 0.99:
            # single molecules
            for s in segments:
                if cut[n[s]] > 0:
                    iVar = n[s]
                    break
            KN_add_con_linear_struct(
                kc, indexCons=count, indexVars=iVar, coefs=1)
            KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=0)
            count += 1
        else:
            raise Exception(
                "Could not create integer cut constraints for single molecules.")

    # Fix binary variable for not allowed molecular structures to 0
    for s in structures:
        if s not in allowed_structures:
            KN_add_con_linear_struct(
                kc, indexCons=count, indexVars=y[s], coefs=1)
            KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=0)
            count += 1

    # Forbid segments
    for s in camd_parameters.forbid_seg:
        iCon = count
        iVar = n[s]
        KN_add_con_linear_struct(kc, indexCons=iCon, indexVars=iVar, coefs=1)
        KN_set_con_eqbnds(kc, indexCons=count, cEqBnds=0)
        count += 1

    return kc


def fix_molecule(x, camd_parameters, process_parameters):

    # start = no pdof
    start = process_parameters.n_pdof
    structures = camd_parameters.all_structures
    segments = camd_parameters.all_segments
    n = camd_parameters.n
    y = camd_parameters.y

    # Bounds from parameters
    yLoBnds = camd_parameters.yLoBnds
    yUpBnds = camd_parameters.yUpBnds
    nLoBnds = camd_parameters.nLoBnds
    nUpBnds = camd_parameters.nUpBnds

    # Fix structures
    for s in structures:
        yLoBnds[y[s] - start] = x[y[s]]
        yUpBnds[y[s] - start] = x[y[s]]

    start = start + camd_parameters.n_struct
    # Fix number of segments
    for s in segments:
        nLoBnds[n[s] - start] = x[n[s]]
        nUpBnds[n[s] - start] = x[n[s]]

    # Bounds from parameters
    camd_parameters.yLoBnds = yLoBnds
    camd_parameters.yUpBnds = yUpBnds
    camd_parameters.nLoBnds = nLoBnds
    camd_parameters.nUpBnds = nUpBnds

    return camd_parameters
