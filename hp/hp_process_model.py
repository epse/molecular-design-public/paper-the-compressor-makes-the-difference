from feos import *
from feos.si import *
from feos.eos import State, PhaseEquilibrium

import numpy as np
from molecule import state_equation
try:
    import detailed_compressor
except:
    pass



def process_model(x, eos, sp_values, jp_values, process_parameters, compressor_parameters):

    
    ## PRE-DECLARATION ##
    state = {}
    T = {}
    h = {}
    h_mol = {}
    s = {}
    s_mol = {}
    p = {}
    steam_quality = {}

    ## VARIABLES ##
    # Convert variables to format that suits process
    # Process degrees of freedom
    T_evap = x[0] * KELVIN
    T_cond = x[1] * KELVIN
    deltaT_sh = x[2] * KELVIN
    deltaT_sc = x[3] * KELVIN
    D = x[4] * 10 * MILLI * METER  # diameter scaled with 10 mm

    # Try-except structure to cope with non-converging states
    try:

        ### PROCESS ###

        ## VLE ##
        # Vapor-liquid equilibriums
        try:
            vle_cond = PhaseEquilibrium.pure(
                eos, temperature_or_pressure=T_cond, max_iter=100, tol=1e-6)
        except:
            raise Exception("")
        try:
            vle_evap = PhaseEquilibrium.pure(
                eos, temperature_or_pressure=T_evap, max_iter=100, tol=1e-6)
        except:
            raise Exception("")
        # VLE pressures
        try:
            p_cond = vle_cond.liquid.pressure()
            p_evap = vle_evap.liquid.pressure()
        except:
            raise Exception("")

        ## STATE 3 ##
        # after condenser, supercooled, liquid
        T["3"] = T_cond - deltaT_sc
        p["3"] = p_cond
        try:
            state["3"] = State(eos, temperature=T["3"],
                               pressure=p["3"], density_initialization='liquid')
            h["3"] = state["3"].specific_enthalpy()
            h_mol["3"] = state["3"].molar_enthalpy()
        except:
            raise Exception("")

        ## STATE 4 ##
        # after throttle, isenthalpic to state 3
        # if state is in 2-phase region, PC-SAFT only computes meta-stable states
        # thus, state cannot be set and state variables must be computed with help of steam quality and dew/bubble point
        h["4"] = h["3"]
        h_mol["4"] = h_mol["3"]
        p["4"] = p_evap
        # Calculate steam quality
        steam_quality["4"] = np.round((h["4"]-vle_evap.liquid.specific_enthalpy())/(
            vle_evap.vapor.specific_enthalpy()-vle_evap.liquid.specific_enthalpy()), 4)
        if steam_quality["4"] >= 0:
            # 2-phase region
            state["4"] = {}
            state["4"]["T"] = T_evap
            state["4"]["s"] = vle_evap.liquid.specific_entropy(
            ) + (vle_evap.vapor.specific_entropy() - vle_evap.liquid.specific_entropy()) * steam_quality["4"]
            state["4"]["s_mol"] = vle_evap.liquid.molar_entropy(
            ) + (vle_evap.vapor.molar_entropy() - vle_evap.liquid.molar_entropy()) * steam_quality["4"]
            state["4"]["h"] = h["4"]
            state["4"]["p"] = p_evap
            T["4"] = T_evap
        else:
            # liquid
            try:
                state["4"] = State(eos, initial_temperature=T_evap-20*KELVIN,
                                   pressure=p["4"], enthalpy=h_mol["4"], density_initialization='liquid')
                T["4"] = state["4"].temperature
            except:
                raise Exception("")

        ## STATE 1 ##
        # after evaporator, superheating deltaT_sh, gaseous
        T["1"] = T_evap + deltaT_sh
        p["1"] = p_evap
        try:
            state["1"] = State(eos, temperature=T["1"],
                               pressure=p["1"], density_initialization='vapor')
            s["1"] = state["1"].specific_entropy()
            s_mol["1"] = state["1"].molar_entropy()
            h["1"] = state["1"].specific_enthalpy()
            h_mol["1"] = state["1"].molar_enthalpy()
        except:
            raise Exception("")

        ## TEMPERATURE ESTIMATION ##
        # Linearize condenser isobaric of gas to estimate initial temperatures for state iteration
        # h,s- T function
        # l: dew point
        T_l = T_cond
        h_l = vle_cond.vapor.specific_enthalpy()
        s_l = vle_cond.vapor.specific_entropy()
        T_h = T_l + 20*KELVIN
        state_h = State(eos, temperature=T_h, pressure=p_cond,
                        density_initialization='vapor')
        h_h = state_h.specific_enthalpy()
        s_h = state_h.specific_entropy()

        if compressor_parameters.model != "detailed":

            ## STATE 2s ##
            # after compressor, isentropic to state 1
            p["2s"] = p_cond
            # Calculate steam quality
            steam_quality["2s"] = (s["1"]-vle_cond.liquid.specific_entropy())/(
                vle_cond.vapor.specific_entropy()-vle_cond.liquid.specific_entropy())
            if steam_quality["2s"] <= 1:
                # 2-phase region
                state["2s"] = {}
                state["2s"]["T"] = T_cond
                state["2s"]["h"] = vle_cond.liquid.specific_enthalpy(
                ) + (vle_cond.vapor.specific_enthalpy() - vle_cond.liquid.specific_enthalpy()) * steam_quality["2s"]
                state["2s"]["s"] = s["1"]
                state["2s"]["s_mol"] = s_mol["1"]
                state["2s"]["p"] = p_cond
                state["2s"]["cp_iG"] = vle_cond.vapor.c_p(
                    Contributions.IdealGas)
                h["2s"] = state["2s"]["h"]
                h_mol["2s"] = vle_cond.liquid.molar_enthalpy(
                ) + (vle_cond.vapor.molar_enthalpy() - vle_cond.liquid.molar_enthalpy()) * steam_quality["2s"]
            else:
                # gas
                # estimate for T_2s
                T_2s_estimate = T_l + (T_h - T_l) * (s["1"]-s_l) / (s_h-s_l)
                try:
                    state["2s"] = State(eos, initial_temperature=T_2s_estimate,
                                        pressure=p["2s"], molar_entropy=s_mol["1"], density_initialization='vapor')
                    h["2s"] = state["2s"].specific_enthalpy()
                    h_mol["2s"] = state["2s"].molar_enthalpy()
                except:
                    raise Exception("")

            ## ISENTROPIC COMPRESSOR EFFICIENCY ##
            # Get isentropic efficiency
            eta_sC = compressor_parameters.eta_sC

            # Enthalpy after compressor
            # real state after compressor (using isentropic efficiency)
            h["2"] = h["1"] + (1/eta_sC) * (h["2s"]-h["1"])
            h_mol["2"] = h_mol["1"] + (1/eta_sC) * (h_mol["2s"]-h_mol["1"])

            # Estimate temperature after compressor
            # Calculate steam quality
            steam_quality["2"] = (h["2"]-vle_cond.liquid.specific_enthalpy())/(
                vle_cond.vapor.specific_enthalpy() - vle_cond.liquid.specific_enthalpy())
            # estimate for T_2
            T_2_estimate = T_l + (T_h - T_l) * (h["2"]-h_l) / (h_h-h_l)

        if compressor_parameters.model == "detailed":
            # Detailed compressor model
            # Extract saft parameter values from saft parameters
            try:
                simulation = detailed_compressor.Simulation(sp_values, jp_values, T["1"] / KELVIN, p_evap / (KILO*PASCAL), p_cond / (
                    KILO * PASCAL), 315., D / (MILLI * METER), compressor_parameters.IS, compressor_parameters.max_error, compressor_parameters.max_iterations)
                # Compressor mass flow
                m_dot_comp = simulation.m_dot

                h["2"] = simulation.h_out
                T_2_estimate = simulation.t_out
                eta_sC = simulation.eta_sc
            except:
                raise Exception("")
            # Add units
            h["2"] = h["2"] * KILO * JOULE / KILOGRAM
            T_2_estimate = T_2_estimate * KELVIN
            m_dot_comp = m_dot_comp * KILOGRAM / SECOND
            # Convert enthalpy to molar enthalpy
            h_mol["2"] = h["2"] * state_equation.get_molarweight(eos)
            # Calculate steam quality
            steam_quality["2"] = (h["2"]-vle_cond.liquid.specific_enthalpy())/(
                vle_cond.vapor.specific_enthalpy() - vle_cond.liquid.specific_enthalpy())

        ## STATE 2 ##
        p["2"] = p_cond
        if steam_quality["2"] <= 1:
            # 2-phase region
            state["2"] = {}
            state["2"]["T"] = T_cond
            state["2"]["s"] = vle_cond.liquid.specific_entropy(
            ) + (vle_cond.vapor.specific_entropy() - vle_cond.liquid.specific_entropy()) * steam_quality["2"]
            state["2"]["s_mol"] = vle_cond.liquid.molar_entropy(
            ) + (vle_cond.vapor.molar_entropy() - vle_cond.liquid.molar_entropy()) * steam_quality["2"]
            state["4"]["h"] = h["2"]
            state["2"]["p"] = p_cond
            T["2"] = T_cond
        else:
            # gas
            try:
                state["2"] = State(eos, initial_temperature=T_2_estimate, pressure=p["2"],
                                   molar_enthalpy=h_mol["2"], density_initialization='vapor')
                T["2"] = state["2"].temperature
            except:
                raise Exception("")

        ## MASS FLOW OF WORKING FLUID AND HEATING POWER ##
        if process_parameters.fixed_heat:
            delta_h_cond = (h["2"] - h["3"])
            if compressor_parameters.model == "detailed":
                # Total mass flow (mass flow one piston * number of pistons)
                n_piston = compressor_parameters.n_piston
                m_dot = n_piston * m_dot_comp
                heating_power = m_dot * delta_h_cond
                # Compute difference of heating power to target heating power$
                Q_cond = process_parameters.Q_heat
                delta_heating_power = (
                    heating_power - Q_cond) / (KILO*WATT)
            else:
                # Some alternative values
                Q_cond = process_parameters.Q_heat
                heating_power = Q_cond
                m_dot = Q_cond / delta_h_cond
                n_piston = 0
                delta_heating_power = 0
        else:
            n_piston = compressor_parameters.n_piston
            m_dot = m_dot_comp * n_piston
            Q_cond = m_dot * (h["2"] - h["3"])

        # Calculate volumetric capacity
        # Refrigeration effect (in this case: heating power) / volumetric unit of fluid entering the compressor (state 1) in MJ /m3
        # Q_vol = Q_cond / V_dot = m_dot * (h2-h3) / (m_dot / rho_1) = (h2-h3) / rho1
        density_in_comp = state["1"].mass_density()
        Q_vol = heating_power / (m_dot / density_in_comp)

        ## COEFFICIENT OF PERFORMANCE ##
        COP = (h["2"]-h["3"])/(h["2"]-h["1"])

        ## COMPRESSION CONSTRAINTS ##
        # These constraints ensure that compression doesn't interfere with 2-phase region.
        compression_constraints = []
        if process_parameters.n_compression_con > 0:

            step = (p_cond-p_evap) / (KILO * PASCAL) / \
                process_parameters.n_compression_con
            pressures = []
            p_0 = p_evap + step * (KILO * PASCAL)

            for i in range(process_parameters.n_compression_con-1):
                p = p_0 + i * step * (KILO * PASCAL)
                pressures.append(p)

            for p_temp in pressures:
                try:
                    vle_temp = PhaseEquilibrium.pure(
                        eos, temperature_or_pressure=p_temp, max_iter=100, tol=1e-6)
                    dew_temp = vle_temp.vapor
                    bub_temp = vle_temp.liquid
                    T_dew = dew_temp.temperature
                    h_dew = dew_temp.specific_enthalpy()
                except:
                    raise Exception("")
                steam_q_s = np.round((s["1"]-bub_temp.specific_entropy())/(
                    dew_temp.specific_entropy()-bub_temp.specific_entropy()), 4)
                if steam_q_s <= 1:
                    # 2-phase region
                    h2s_temp = bub_temp.specific_enthalpy() + (dew_temp.specific_enthalpy() -
                                                               bub_temp.specific_enthalpy()) * steam_q_s
                else:
                    # gas
                    # Linearize condenser isobaric of gas to estimate initial temperatures for state iteration
                    # h,s- T function
                    # l: dew point
                    T_l = T_dew
                    s_l = dew_temp.specific_entropy()
                    # h: see above, condenser pressure
                    try:
                        state_h = State(
                            eos, temperature=T_h, pressure=p_temp, density_initialization='vapor')
                        s_h = state_h.specific_entropy()
                    except:
                        raise Exception("")
                    # estimate for T_2s
                    T_2s_estimate = T_l + (T_h - T_l) * \
                        (s["1"]-s_l) / (s_h-s_l)
                    try:
                        state_2s_temp = State(eos, initial_temperature=T_2s_estimate,
                                              pressure=p_temp, entropy=s_mol["1"], density_initialization='vapor')
                        h2s_temp = state_2s_temp.specific_enthalpy()
                    except:
                        raise Exception("")
                h2_temp = h["1"] + (1/eta_sC) * (h2s_temp-h["1"])

                con_temp = (h2_temp - h_dew) / (KILO * JOULE / KILOGRAM)
                compression_constraints.append(con_temp)
            # check end state
            con_temp = (h["2"] - vle_cond.vapor.specific_enthalpy()
                        ) / (KILO * JOULE / KILOGRAM)
            compression_constraints.append(con_temp)

        ## PINCH CONSTRAINTS ##

        # Condenser <-> heat sink
        # Possible points of heat sink temperature profile
        # Condenser outlet
        DeltaT_cond_out = T["3"] - process_parameters.T["sink"]["in"]
        # Condenser inlet
        DeltaT_cond_in = T["2"] - process_parameters.T["sink"]["out"]
        # Dew point
        T_sink_dew = process_parameters.T["sink"]["out"] - (h["2"] - vle_cond.vapor.specific_enthalpy())/(
            h["2"]-h["3"]) * (process_parameters.T["sink"]["out"]-process_parameters.T["sink"]["in"])
        DeltaT_cond_dew = vle_cond.vapor.temperature - T_sink_dew

        # Evaporator <-> heat source
        # assumed as isothermic heat source for now
        if process_parameters.heat_source == "isothermic":
            # Evaporator outlet
            DeltaT_evap_out = process_parameters.T["source"]["in"] - T["1"]
            # Evaporator inlet
            DeltaT_evap_in = process_parameters.T["source"]["in"] - T["4"]
            state["source,in"] = {}
            state["source,out"] = {}
            state["source,in"]["T"] = process_parameters.T["source"]["in"]
            state["source,out"]["T"] = process_parameters.T["source"]["in"]
        elif process_parameters.heat_source == "water":
            # Calculate evaporator heat:
            Q_dot_evap = m_dot * (h["1"] - h["4"])
            state["source,in"] = {}
            state["source,out"] = {}
            state["source,in"]["T"] = process_parameters.T["source"]["in"]
            state["source,out"]["T"] = process_parameters.T["source"]["in"] - \
                Q_dot_evap / \
                (process_parameters.m_dot["source"] * process_parameters.cp_w)
            # Evaporator outlet (warm side)
            DeltaT_evap_out = state["source,in"]["T"] - T["1"]
            # Evaporator inlet (cold side)
            DeltaT_evap_in = state["source,out"]["T"] - T["4"]
        else:
            raise Exception(
                "Error! Could not find nature of heat source. Please specify either water or isothermic.")

        pinch_constraints = [DeltaT_cond_out / KELVIN, DeltaT_cond_in / KELVIN,
                             DeltaT_cond_dew / KELVIN]

        ## PRESSURE CONSTRAINTS ##
        try:
            state_cp = State.critical_point(eos)
        except:
            raise Exception("")

        p_max = process_parameters.p["max,red"] * state_cp.pressure()
        pressure_constraints = []

        # Evaporator pressure
        pressure_constraints.append((p_max - p_evap) / BAR)
        pressure_constraints.append(
            (p_evap - process_parameters.p["min,abs"]) / BAR)
        # Condenser pressure
        pressure_constraints.append((p_max - p_cond) / BAR)
        pressure_constraints.append(
            (p_cond - process_parameters.p["min,abs"]) / BAR)

        ## HEATING POWER CONSTRAINT ##
        heating_power_con = [delta_heating_power]

        constraints = pinch_constraints + compression_constraints + \
            pressure_constraints + heating_power_con

    except:

        raise Exception("")

    return COP, constraints, state, eta_sC, heating_power, m_dot, n_piston, Q_vol