# Script to update molecular database, e.g. if new segments or single molecules have been added

#import manager
import json, os
import numpy as np
from molecule import camd_parameters

## LOAD DATABASE ##
# Name of database
database_file = "molecule_database/database.json"

# Load camd parameters
camd_parameters = camd_parameters.camd_parameters("hp")

# Read database from file
# Format: two lists, first list stores names, second list stores segments of each molecule
with open(database_file) as f:
    db = json.load(f)

# Names: 1-d list with molecule names
names = db[0]
# Segments: n-d list with segments
seg_list = db[1]

# Check if length is uniform
length_db = len(seg_list[0])
for i, s in enumerate(seg_list):
    if len(s) != length_db:
        raise Exception("Length of molecular configurations in database is not uniform. Look at molecule " + names[i])

# Check if new segments have been added and how many
# How many new segments?
no_new = len(camd_parameters.all_segments) - length_db
if no_new == 0:
    raise Exception("You do not need to update the database, because no new segments have been added.")

# names new segments
new_names = camd_parameters.all_segments[length_db:len(camd_parameters.all_segments)]

for i, new in enumerate(new_names):
    # Append a zero for all existing molecules
    for s in seg_list:
        s.append(0)
    
# Ask if new segment shall be added as new molecule
for k, new in enumerate(new_names):
    add_molecule = input("Shall the new segment " + new + " be added as a single molecule to the database? Type in y to do so, otherwise press any other key.")
    if add_molecule == "y":
        # Ask for name
        name = input("Enter name.")
        # Catch exception for too short input
        if len(name) < 4:
            raise Exception("Entered molecule name is too short, there must be a typo.")

        # Create new molecular configuration
        temp = [0 for j in range(len(seg_list[0]))]
        last = length_db + k
        temp[last] = 1
        # Check if name is already in database
        if name in names:
            delete = input("Molecule is already in database. Do you want to delete old configuration? Type in y to do so, otherwise press any other key.")
            if delete == "y":
                ind = names.index(name)
                names.remove(name)
                seg_config_to_remove = seg_list[ind]
                seg_list.remove(seg_config_to_remove)
        names.append(name)
        seg_list.append(temp)
    

# Save database
# New Database
new_db = [names, seg_list]

# Save new database to file
filename = "molecule_database/database.json"
x_save = json.dumps(new_db)
open(filename, "w").write(x_save)

# Print success
print("Database updated successfully.")






