from feos.si import *
import math
import numpy as np


class process_parameters:

    def __init__(self):
        """ Initialization of process parameters.
        """

        # number of process degrees of freedom
        self.n_pdof = 5

        # number of points during compression where 2-phase region is checked
        self.n_compression_con = 1
        # number of critical points regarding pinch
        self.n_pinch_con = 3
        # additional process constraints
        self.n_process_con = 1
        self.n_pressure_con = 4                                # pressure constraints
        # Constrained heating power
        self.n_heating_pow_con = 1

        self.R = 8.314 * JOULE / MOL / KELVIN                   # universal gas constant

        # heat capacity at constant pressure of water (estimate)
        self.cp_w = 4.2 * KILO * JOULE / KILOGRAM / KELVIN
        self.cp_l = 1.05 * KILO * JOULE / KILOGRAM / KELVIN

        # Heat sink and source
        self.T = {}
        self.T["sink"] = {}
        # inlet temperature of heat sink: return line of heating system
        self.T["sink"]["in"] = (273.15 + 25) * KELVIN
        # outlet temperature of heat sink: supply line of heating system
        self.T["sink"]["out"] = (273.15 + 35) * KELVIN
        # Nature of heat source
        self.heat_source = "isothermic"
        #self.heat_source = "water"
        self.T["source"] = {}
        if self.heat_source == "isothermic":
            # isothermic heat source: constant source temperature
            self.T["source"]["in"] = (273.15 + 5) * KELVIN
        elif self.heat_source == "water":
            # incoming temperature of heat source (water)
            self.T["source"]["in"] = (273.15+17)*KELVIN
        self.m_dot = {}
        self.m_dot["source"] = 7.5/60 * KILOGRAM / SECOND

        # Maximum/minimum pressure
        self.p = {}
        # maximum reduced pressure
        self.p["max,red"] = 0.8
        # minimum absolute pressure
        self.p["min,abs"] = 1 * BAR

        # Default evaporation and compression temperature for simulation
        self.T["evap"] = 273.15 * KELVIN
        self.T["cond"] = 333.15 * KELVIN

        # pinch constraints
        self.DeltaT = {}
        # minimum temperature temperature difference
        self.DeltaT["min"] = 1 * KELVIN

        # maximum/minimum temperature for evaporation and condensation
        self.T["max"] = self.T["sink"]["out"] + \
            30 * KELVIN     # maximum temperature
        # minimum temperature
        self.T["min"] = 250 * KELVIN

        # Bounds
        self.DeltaT["sh,min"] = 5 * KELVIN
        self.DeltaT["sh,max"] = 5 * KELVIN
        self.DeltaT["sc,min"] = 0 * KELVIN
        self.DeltaT["sc,max"] = 15 * KELVIN
        self.T["evap,min"] = self.T["source"]["in"] - \
            self.DeltaT["sh,min"] - self.DeltaT["min"]
        self.T["evap,max"] = self.T["source"]["in"] - \
            self.DeltaT["sh,min"] - self.DeltaT["min"]
        self.T["cond,min"] = self.T["sink"]["in"]
        self.T["cond,max"] = self.T["sink"]["out"] + 50 * KELVIN

        d_min = 2.5
        d_max = 9

        # Bound vector
        self.xLoBnds = [self.T["evap,min"] / KELVIN, self.T["cond,min"] /
                        KELVIN, self.DeltaT["sh,min"] / KELVIN, self.DeltaT["sc,min"] / KELVIN, d_min]
        self.xUpBnds = [self.T["evap,max"] / KELVIN, self.T["cond,max"] /
                        KELVIN, self.DeltaT["sh,max"] / KELVIN, self.DeltaT["sc,max"] / KELVIN, d_max]

        # Initial values for process degrees of freedom
        self.x_init = [self.T["evap,min"] / KELVIN, (self.T["cond,min"]+self.T["cond,max"])
                       * 0.5 / KELVIN, self.DeltaT["sh,min"] / KELVIN, self.DeltaT["sc,min"] / KELVIN]
        self.x_init = [self.T["evap,min"] / KELVIN, self.T["cond,min"] / KELVIN +
                       20, self.DeltaT["sh,min"] / KELVIN, self.DeltaT["sc,min"] / KELVIN, 3.0]

        # Case study: fix heat?
        self.fixed_heat = 1
        self.Q_heat = 6 * KILO * WATT


class compressor_parameters:

    def __init__(self, compressor_model):

        # Compressor model to be used: constant, detailed
        if compressor_model not in ["constant", "detailed"]:
            raise Exception("Specified compressor model unknown.")
        else:
            self.model = compressor_model

        # Default compressor efficiency
        self.eta_sC = 0.6

        # Number of pistons
        self.n_piston = 2

        # For detailed model without units

        self.IS = 5000
        self.max_error = 0.0001
        self.max_iterations = 50
