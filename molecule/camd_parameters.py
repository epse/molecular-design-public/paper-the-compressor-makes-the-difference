from hp import hp_parameters
import numpy as np
import json
import pandas as pd


class camd_parameters:

    """ CAMD PARAMETERS

    This class defines the parameters that are necessary for CAMD (optimization or simulation).
    It contains information on the segments, possible molecular structure, as well as help to 
    build the basis for the variables (e.g. variable pointers to facilitate model set-up)
    and the constraints.

    """

    def __init__(self, tech):

        ####################### SEGMENTS #########################

        # Read information about segments from xlsx-file
        xls_filename = "molecule/parameters_homo_segments.xlsx"
        dfs = pd.read_excel(xls_filename, sheet_name=None)[
            'Segment Information']

        # Write to needed data structures
        sp = ['m', 'sigma', 'epsilon_k', 'mu', 'epsilon_k_ab', 'kappa_ab', 'q',
              'molarweight', 'n_max', 'n_openBonds', 'name', 'a', 'b', 'c', 'd', 'e']

        # List with all segments
        self.all_segments = []
        self.segment_param = {}
        self.molecules = []
        for i, row in dfs.iterrows():
            # Name
            ident = row["identifier"]
            self.all_segments.append(ident)
            # Saft parameters
            self.segment_param[ident] = {}
            for p in sp:
                temp = row[p]
                if p != "name":
                    if np.isnan(temp):
                        temp = 0
                self.segment_param[ident][p] = temp
            # Identify single molecules
            if self.segment_param[ident]["n_openBonds"] == 0:
                self.molecules.append(row["identifier"])
        # Amount of segments
        self.n_seg = len(self.all_segments)

        # Prepare bounds for optimization
        self.nLoBnds = np.zeros(self.n_seg)
        self.nUpBnds = np.zeros(self.n_seg)
        for i, s in enumerate(self.all_segments):
            self.nUpBnds[i] = self.segment_param[s]["n_max"]

        # maximum/minimum total number of groups
        self.n_tot_max = 25
        self.n_tot_min = 1

        # Optional: Forbid segments
        self.forbid_seg = ["OH", "NH2"]
        self.forbid_seg = ["C3H4"]

        ####################### STRUCTURES #########################

        # All structures
        self.all_structures = ["alkane", "alkene",
                               "arom", "hex", "pent", "P", "M"]
        self.long_structures = {"alkane": "Alkane", "alkene": "Alkene", "arom": "Aromatics",
                                "hex": "Cyclohexanes", "pent": "Cyclopentanes", "P": "P/A/Alkyne", "M": "Single Molecule"}
        # Allowed structures
        self.allowed_structures = self.all_structures
        # self.allowed_structures = [
        #    "alkane", "alkene", "arom", "hex", "pent", "P", "M"]

        # Number of structures
        self.n_struct = len(self.all_structures)
        # Number of allowed structures
        self.n_allowed_struct = len(self.allowed_structures)
        # Prepare bounds for optimization: binary variables
        self.yLoBnds = np.zeros(self.n_struct)
        self.yUpBnds = np.ones(self.n_struct)

        ####################### VARIABLE POINTERS #########################

        # Structures
        self.y = {}
        if tech == "hp":
            pp = hp_parameters.process_parameters()
            count = pp.n_pdof
        for i in self.all_structures:
            self.y[i] = count
            count = count + 1

        # Segments
        self.n = {}
        for i in self.all_segments:
            self.n[i] = count
            count = count + 1

        ####################### CAMD CONSTRAINTS #########################

        # Number of CAMD constraints
        self.n_camd_constraints = 19 + \
            len(self.forbid_seg) + self.n_struct - \
            self.n_allowed_struct + self.n_seg
