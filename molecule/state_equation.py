from feos import *
from numpy.core.fromnumeric import compress
from feos.si import *
from feos.pcsaft import *
from feos.eos import EquationOfState, State

import numpy as np
import math
import json
from molecule import database


def create_eos(saft_parameters):

    return EquationOfState.pcsaft(saft_parameters)


def extract_saft_param_values(saft_parameters):

    saft_record = saft_parameters.pure_records[0].saft_record
    m = saft_record.m
    sigma = saft_record.sigma
    epsilon_k = saft_record.epsilon_k
    mu = saft_record.mu
    if mu == None:
        mu = 0
    kappa_ab = saft_parameters.pure_records[0].saft_record.kappa_ab
    if kappa_ab == None:
        kappa_ab = 0
    epsilon_k_ab = saft_parameters.pure_records[0].saft_record.epsilon_k_ab
    if epsilon_k_ab == None:
        epsilon_k_ab = 0
    molarweight = saft_parameters.pure_records[0].chemical_record.molarweight

    saft_param_values = [m,
                         sigma,
                         epsilon_k,
                         mu,
                         kappa_ab,
                         epsilon_k_ab,
                         molarweight]

    return saft_param_values


def get_joback_parameters(x, camd_parameters):

    # Segment parameters: segment_param["CH3"]["a"] contains a_CH3
    segment_param = camd_parameters.segment_param
    n = camd_parameters.n
    molecules = camd_parameters.molecules   # single molecules
    # all segments: functional groups and single molecules
    segments = camd_parameters.all_segments

    # Joback formulation in PC-SAFT: c_p = a + b * T + c * T^2 + d * T^3 + e * T^4
    # Group contribution method for Joback parameters
    # Joback for segments: sum(n_s * joback-param_s) + const * (1 - y_molecule)
    # Joback for single molecules: sum(n_m * param_m)

    # a
    a = sum(x[n[i]] * segment_param[i]["a"]
            for i in segments) - 37.93 * (1 - sum(x[n[i]] for i in molecules))
    # b
    b = sum(x[n[i]] * segment_param[i]["b"]
            for i in segments) + 0.21 * (1 - sum(x[n[i]] for i in molecules))
    # c
    c = sum(x[n[i]] * segment_param[i]["c"] for i in segments) - \
        3.91e-4 * (1 - sum(x[n[i]] for i in molecules))
    # d
    d = sum(x[n[i]] * segment_param[i]["d"] for i in segments) + \
        2.06e-7 * (1 - sum(x[n[i]] for i in molecules))
    # e
    e = sum(x[n[i]] * segment_param[i]["e"] for i in segments)

    jpvalues = [a, b, c, d, e]

    return jpvalues


def create_parameters(x, process_parameters, camd_parameters):

    # Read camd parameters
    segments = camd_parameters.all_segments
    n = camd_parameters.n
    segment_param = camd_parameters.segment_param

    # Calculation of m
    m = sum(segment_param[s]['m']*x[n[s]] for s in segments)
    # Calculation of m*sigma^3
    msigma3 = sum(segment_param[s]['m']*segment_param[s]
                  ['sigma']**3 * x[n[s]] for s in segments)
    sigma = ((msigma3)/m)**(1/3)
    # Calculation of m*epsilon
    mepsilon = sum(segment_param[s]['m']*segment_param[s]
                   ['epsilon_k']*x[n[s]] for s in segments)
    epsilon_k = (mepsilon/m)
    # Calculation of mu
    mu = sum(segment_param[s]['mu']*x[n[s]] for s in segments)
    # Calculation of epsilon_k_ab
    epsilon_k_ab = sum(
        segment_param[s]['epsilon_k_ab']*x[n[s]] for s in segments)
    # Calculation of kappa
    kappa = sum(segment_param[s]['kappa_ab']*x[n[s]] for s in segments)
    # Calculation of q
    q = sum(segment_param[s]['q']*x[n[s]] for s in segments)
    # Calculation of Molarweight
    molarweight = sum(
        segment_param[s]['molarweight']*x[n[s]] for s in segments)

    # Create Joback record from Joback parameter values
    jp = get_joback_parameters(x, camd_parameters)
    jr = JobackRecord(*jp)

    # Create necessary records
    identifier = Identifier(cas="00-00-0")
    sr = PcSaftRecord(m=m, sigma=sigma, epsilon_k=epsilon_k, mu=mu,
                      q=q, epsilon_k_ab=epsilon_k_ab, kappa_ab=kappa)
    pr = PureRecord(identifier=identifier,
                    molarweight=molarweight, model_record=sr, ideal_gas_record=jr)

    # Create saft parameters from pure record
    saft_param = PcSaftParameters.new_pure(pure_record=pr)
    # Save values of saft parameters
    sp_values = [m,
                 sigma,
                 epsilon_k,
                 mu,
                 kappa,
                 epsilon_k_ab,
                 molarweight]

    return saft_param, sp_values, jp


def get_x_mol_init(x_seg, process_parameters, camd_parameters):

    n = camd_parameters.n
    y = camd_parameters.y

    x_init = [*np.zeros(process_parameters.n_pdof +
                        camd_parameters.n_struct), *x_seg]

    size_x = len(x_init)

    # Find out molecular structure
    alkenes = ['dCH2', 'dCH', 'dC<']
    aromatics = ['CH_arom', 'C_arom']
    hexanes = ['CH2_hex', 'CH_hex']
    pentanes = ['CH2_pent', 'CH_pent']
    polar = ['CHdO', '>CdO', 'OCH3', 'OCH2',
             'HCOO', 'COO', 'OH', 'NH2', 'CtCH']
    molecules = camd_parameters.molecules
    # Alkanes (only CH3 and CH2 groups)
    if (x_init[n["CH3"]] + x_init[n["CH2"]]) == sum(x_init[n["CH3"]:size_x]):
        x_init[y["alkane"]] = 1
    elif sum(x_init[n[a]] for a in alkenes) > 0:
        x_init[y["alkene"]] = 1
    elif sum(x_init[n[a]] for a in aromatics) > 0:
        x_init[y["arom"]] = 1
    elif sum(x_init[n[h]] for h in hexanes) > 0:
        x_init[y["hex"]] = 1
    elif sum(x_init[n[p]] for p in pentanes) > 0:
        x_init[y["pent"]] = 1
    elif sum(x_init[n[p]] for p in polar) > 0:
        x_init[y["P"]] = 1
    elif sum(x_init[n[m]] for m in molecules) > 0:
        x_init[y["M"]] = 1
    else:
        print("Couldn't find matching molecular structure!")

    return x_init[process_parameters.n_pdof:size_x]


def segment_saft_parameters_from_json(camd_parameters):

    # Read segment parameters from file
    n = camd_parameters.n
    segment_file = "molecule/parameters_homo_segments.json"
    with open(segment_file) as f:
        segment_records = json.load(f)

    segment_saft_param = {}
    names = ['m', 'sigma', 'epsilon_k', 'mu', 'epsilon_k_ab', 'kappa_ab']

    for s in segment_records:
        saft_record = s['saft_record']
        ident = s['identifier']
        segment_saft_param[ident] = {}
        for n in names:
            try:
                segment_saft_param[ident][n] = saft_record[n]
            except:
                segment_saft_param[ident][n] = 0
        segment_saft_param[ident]['molarweight'] = s['molarweight']

    return segment_saft_param


def get_x_init(molecule, process_parameters, camd_parameters):

    # Segment configuration from name (only n[s] for all segments)
    x_seg = database.get_segment_config_from_name(molecule, camd_parameters)
    # Structure (contains y[structure] and n[segment])
    x_mol_init = get_x_mol_init(x_seg, process_parameters, camd_parameters)
    # x
    x_init = [*process_parameters.x_init, *x_mol_init]

    return x_init


def get_segments_from_x(x, process_parameters, camd_parameters):

    # Modify segments according to molecular structure contained in x
    segments = {}
    v_count = process_parameters.n_pdof + camd_parameters.n_struct
    for i in camd_parameters.all_segments.keys():
        segments[i] = x[v_count]
        v_count = v_count + 1

    return segments


def get_molarweight(eos):

    state_temp = State(eos, temperature=273.15*KELVIN,
                       pressure=1*BAR, total_moles=1*MOL)
    M_temp = state_temp.mass() / KILOGRAM

    M = M_temp[0] * KILOGRAM / MOL

    return M
