# Refrigerant Selection for Heat Pumps: the Compressor Makes the Difference

In this repository, you can find the framework developed for the paper ["Refrigerant Selection for Heat Pumps: the Compressor Makes the Difference"](https://onlinelibrary.wiley.com/doi/10.1002/ente.202201403) by Lisa Neumaier, Dennis Roskosch, Johannes Schilling, Gernot Bauer, Joachim Gross, and André Bardow published in *Energy Technology*. 

The provided framework calculates a refrigerant ranking for the residential heat pump case study. The compressor can be modeled either with an isentropic efficiency equal for all refrigerants or with a refrigerant dependent compressor model. The framework has been developed and tested on Linux, but should also run on Windows/MacOS. The refrigerant-dependent compressor model implemented in this work is written in [Rust](https://www.rust-lang.org/) and in this work compiled under Linux as a Python package. The compressor model is available in a [separate repository](https://gitlab.ethz.ch/epse/molecular-design-public/paper-the-compressor-makes-the-difference-compressor-design-model). It should generally also be possible to compile it under Windows/MacOS.


## Installation

The framework relies on the commercial MINLP solver provided by [Artelys Knitro](https://www.artelys.com/solvers/knitro/). A Knitro license is necessary to run the code. In this work, we use Knitro version 12.3.0.

Additionally, you need to install the Python package `feos` to compute thermodynamic properties with the PC-SAFT equation of state. This installation is a simple `pip` installation (more information in the [`feos` documentation](https://feos-org.github.io/feos/index.html)). We use `feos 0.4.1`:

```
pip install feos==0.4.1
```

If you want to use the framework with the refrigerant-dependent compressor design model, you need to install the model as a `Python` package. You find the instructions [here](https://gitlab.ethz.ch/epse/molecular-design-public/paper-the-compressor-makes-the-difference-compressor-design-model).


## Calculating rankings

To calculate refrigerant rankings, you need to run the [`MAIN.py`](https://gitlab.ethz.ch/epse/molecular-design-public/paper-the-compressor-makes-the-difference/-/blob/main/MAIN.py) file. 

The following user options are available:
- `tech`: Technology to optimize, currently available: `"hp"`
- `calc_fixed_molecule`: Carry out process optimization for list of fixed molecules, 0 (no) or 1 (yes)
- `molecules_fixed`: List of fixed molecules, e.g., ["Propane"] (Names must be contained in [`database`](https://gitlab.ethz.ch/epse/molecular-design-public/paper-the-compressor-makes-the-difference/-/blob/main/molecule/parameters_homo_segments.xlsx))
- `calc_target`: Calculate target (relaxation of MINLP), 0 (no) or 1 (yes)
- `calc_ranking`: Calculate refrigerant ranking, 0 (no) or 1 (yes)
- `read_int_cut_from_file`: Read integer cut constraints from `.json` file computed during an earlier run. Filename must be provided as `filename_int_cut`.
- `save_solution`: Should solution be saved (1) or not (0)?
- `molecule`: Specify name of molecule to be used as initial value for optimization.
- `fluid_number`: Specify number of ranked refrigerants.
- `compressor_model`: Use equal compressor efficiency for all refrigerants (`"constant"`) or use refrigerant-dependent compressor model (`"detailed"`).
- `filename_target`: You can specify the filename of a previously computed target if you want to warmstart your refrigerant ranking.
- `filename_init_values`: You can specify the filename of initial values if you want to warmstart your optimization.
